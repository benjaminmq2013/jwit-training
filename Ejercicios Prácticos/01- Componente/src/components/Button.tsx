import styled from "styled-components"

interface params {
  title: string,
  backgroundColor?: string;
  iconURL?: string;
}

const Button = styled.button`
  margin-top: 20px;
  border-radius: 100px;
  width: 110px;
  height: 40px;
  color: #fefefe;
  background-color: ${props=> props.color};
  font-weight: 600;
  font-size: 17px;
  border: 0;
  display: flex;
  align-items:center;
  justify-content:space-around;
  padding: 0;
  padding-right: 2px;
  padding-left: 15px;
  cursor: pointer;
  :hover{
    opacity: .9;
  }
`
const Icon = styled.img`
  height: 70%;
  margin-left: 4px;
`

const App = ( params:params ):JSX.Element => {
  return (
    <Button color={ params.backgroundColor }>
      { params.title }
      <Icon src={ params.iconURL }/>
    </Button>
  )
}

export default App
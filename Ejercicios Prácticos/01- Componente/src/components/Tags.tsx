import styled from 'styled-components';
const Container = styled.div`
  width: 100%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  bottom: 100px;
  z-index: 4;
  `
const Tag = styled.span`
   background-color: #fdfbfb;
   border-radius: 20px;
   padding: 10px;
   box-shadow: 0px 0px 6px #8181818e;
   cursor: pointer;
   margin: 0 20px;
`
interface params{
  names: string[],
}


const App = ( params:params ):JSX.Element => {
  const { names } = params;
  return (
    <Container>
      {names.map((name, i) => <Tag>{ name }</Tag>) }
    </Container>    
  )
}


export default App
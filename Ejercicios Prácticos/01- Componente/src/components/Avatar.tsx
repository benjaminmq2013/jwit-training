import styled from 'styled-components';
interface params {
    image: string,
    secondaryColor: string,
}

const Container = styled.figure`
    width: 100%;
    margin: 0;
    display: flex;
    justify-content: right;
    align-items: flex-end;
    position: relative;
`
const Background = styled.div`
    height: 60vh;
    width: 30vw;
    border: 190px solid ${ props => props.color };
    position: absolute;
    bottom: -490px;
    right: -250px;
    border-radius: 500px;
    z-index: 2;
`
const Avatar = styled.img`
    z-index: 3;
`

const App = (params:params) => {
  return (
    <Container>
        <Background color={ params.secondaryColor } />
        <Avatar src={ params.image } />
    </Container>
  )
}

export default App
import styled from "styled-components"
import Card from "./Card"
import { data } from "./data"

const Container = styled.div`
  width: 94vw;
  margin: 0 3vw;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 2vw;
`

const App = ():JSX.Element => {
  return (
    <Container>
      {data.map((data, i) => <Card {...data} key={i} /> )}
    </Container>
  )
}

export default App
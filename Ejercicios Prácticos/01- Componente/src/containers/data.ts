// Información para cada componente <Card />

import ImageA from "../assets/images/avatar-a.png"
import ImageB from "../assets/images/avatar-b.png"

// Interface
import { params } from './Card';

// Data - DB
export const data:params[] = [
    { 
        primaryColor: "#FEC6CB",
        secondaryColor: "#FEA6AD",
        title: "Happier With Robert", 
        subtitle: "204 Episodes", 
        image: ImageA, 
        names: [ "Self Help", "Wellness", "Life" ] 
    },

    { 
        primaryColor:"#A3EDE4", 
        secondaryColor: "#69E4D7", 
        title:"Relationships With Sara", 
        subtitle:"103 Episodes", 
        image: ImageB , 
        names:[ "Relationships", "Couples", "Date" ] 
    }
]

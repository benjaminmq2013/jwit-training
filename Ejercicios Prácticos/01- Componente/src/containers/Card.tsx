import styled from "styled-components"
import Button from "../components/Button"
import Avatar from "../components/Avatar"
import playIcon from "../assets/icons/play.svg"
import Tags from "../components/Tags"

const Container = styled.div`
    background-color: ${ props=> props.color };
    border-radius: 9px;
    padding: 3vh 0 0 3vw;
    overflow: hidden;
    position: relative;
`
const Title = styled.h2`
    margin: 10px 0;
    opacity: .75;
`
const SubTitle = styled.h3`
    color: grey;
    font-weight: 400;
    margin: 10px 0;
`
export interface params{
    primaryColor: string,
    secondaryColor: string,
    title: string,
    subtitle: string,
    image: string,
    names:string[]
}

const App = (params:params) => {
  return (
    <Container color={ params.primaryColor }>
        <Title>{ params.title }</Title>
        <SubTitle>{ params.subtitle }</SubTitle>
        <Button title="Play" backgroundColor="#8A5CF2" iconURL={ playIcon } />
        <Avatar image={ params.image } secondaryColor={ params.secondaryColor } />        
        <Tags names={ params.names } />
    </Container>
  )
}

export default App
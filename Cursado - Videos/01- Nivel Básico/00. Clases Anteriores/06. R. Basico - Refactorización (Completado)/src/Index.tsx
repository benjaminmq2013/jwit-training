// 00:42:00    00:45:07
import styled from "styled-components"
import Menu from "../Components/Menu/src";



const Container = styled.div`
`


function App(): JSX.Element {
  return (
    <Container>
      <Menu
        onAccept={(e) => console.log(e)}
        onCancel={() => console.log("SELECCION CANCELADA")}
        
      />
      <Menu
        backgroundColor="#21d0d0"
        options={["PRICE LOW TO HIGH", "PRICE HIGH TO LOW", "popularity"]}
        onAccept={(e) => console.log(e)}
      />
    </Container>
  );
}

export default App

## Installation & Usage

```sh
npm install @react/menu --save
```

### Include the Component

```js
import App from "@react/template"

const App = () => {
    return <App />
}
```
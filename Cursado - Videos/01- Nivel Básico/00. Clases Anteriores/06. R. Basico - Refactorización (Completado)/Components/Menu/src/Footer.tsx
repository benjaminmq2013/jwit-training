// NOTA: Más tarde voy a refactorizar este componente para no mutar la data
import React from 'react'
import styled from "styled-components";
import Btn from "./Btn"

import { Cancel, Reset, Check } from "./Buttons"
import { resources } from './types';



const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  margin-top: 15px;
  padding-top: 15px;
  padding-bottom: 25px;
  border-top: "1px solid #f4f4f480";
  border-top: 1px solid white;
`;

interface option{
  element: JSX.Element,
  onClick: () => void;
}

export interface params {
  enableReset?: boolean;
  enableCancel?: boolean;
  onReset?: () => void;
  onAccept?: () => void;
  onCancel?: () => void;
};

const App = (params:params):JSX.Element => {

  const handleReset = () => {
    if(typeof params.onReset === "function" ) params.onReset()
    console.log("Reseteando")
  };

  const handleAccept = () => {
    if(typeof params.onAccept === "function" ) params.onAccept()
  };

  const handleCancel = () => {
    console.log("Cancelando")
    if(typeof params.onCancel === "function" ) params.onCancel()
  };

  
  let data:option[] = [  { 
    element: <Check />, 
    onClick:()=>{handleAccept()} 
  }
]

// NOTA: Más tarde voy a refactorizar este componente para no mutar la data

if(params.enableReset) data.push({ 
  element: <Reset />,
  onClick:()=>{handleReset()} 
},)

if(params.enableCancel) data.push({ 
  element: <Cancel />, 
  onClick:()=>{handleCancel()} 
},)

  
  return (
    <Container >
        {data.map((v, i) => <Btn key={ i } element={ v.element } onClick={ v.onClick } /> )}       
    </Container>
  )
}

App.defaultProps = {
  enableReset: true,
  enableCancel: true,
}

export default App
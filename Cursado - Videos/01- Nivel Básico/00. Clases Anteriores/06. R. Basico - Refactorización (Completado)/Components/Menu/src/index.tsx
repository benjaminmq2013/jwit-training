import React from 'react'
import Option, { params as ParamsComponentOption } from "./Option";
import styled from "styled-components"
import Footer from "./Footer"
import { useState, useEffect } from 'react';
import { resources } from './types';

const Container = styled.div`
  margin-top: 15px;
  background-color: ${(props) => props.color};
  border-radius: 15px;

  width: 400px;
  padding-top: 15px;
`


export interface select {
  value: string,
  state: boolean
}

export interface params{
  options: string[]
  backgroundColor?: string,
  onAccept?: (options: select[] ) => void,
  onCancel?: (options: select[] ) => void,
}


const App = (params:params): JSX.Element => {
  
  const [ optionsState, setOptionsState ] = useState<select[]>([])

  useEffect(() => {
    let raw:select[] = [];

    params.options.forEach(el => raw.push({value: el, state: false}))

    setOptionsState(raw)

  }, [params.options])

  const handleClick:ParamsComponentOption["onClick"] = (e) => {

    const copy = [...optionsState]

    const index:number = copy.findIndex((f) => f.value === e.value);

    if (index > -1) copy[index] = e;
    
    setOptionsState(copy)
    
  }

  const handleAccept = () => {
    if (typeof params.onAccept === "function")params.onAccept(optionsState)
  }
  const handleReset = () => {
    let raw:select[] = [];

    params.options.forEach(el => raw.push({value: el, state: false}))

    setOptionsState(raw)
  }
  const handleCancel = () => {
    if (typeof params.onCancel === "function")params.onCancel(optionsState)
  }

  return (
    <Container color={ params.backgroundColor }>
      {optionsState.map((v, i)=> <Option key={i} value={v.value} state={ v.state } onClick={ handleClick } />)}
      <Footer onAccept={handleAccept} onReset={ handleReset } onCancel={ handleCancel } />
    </Container>
  )
}

App.defaultProps = {
  options: ["lup nutrition",
  "asitis",
  "avatar",
  "big musclesularity",
  "bpi sports",
  "bsn",
  "cellucor",
  "domin8r",],
  backgroundColor: "#ff7745"
}

export default App
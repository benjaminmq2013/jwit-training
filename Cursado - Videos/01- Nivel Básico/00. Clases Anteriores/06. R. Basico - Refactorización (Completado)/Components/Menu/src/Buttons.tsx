import React from 'react'
import styled from "styled-components";

const removeURL:string = "/public/images/remove-icon.svg";
const resetURL:string = "/public/images/reset-icon.svg";
const checkURL:string = "/public/images/check-icon.svg";
const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`
const Icon = styled.img`
  height: 20px;
  width: 20px;
`;

export const Cancel = () => {
  return (
    <Icon src={ removeURL } />
  )
};

export const Reset = () => {
    return <Icon src={ resetURL } />
};

export const Check = () => {
    return <Icon src={ checkURL } />
};
import styled from "styled-components";

import removeURL from "../images/remove-icon.svg";
import resetURL from "../images/reset-icon.svg";
import checkURL from "../images/check-icon.svg";
const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`
const Icon = styled.img`
  height: 20px;
  width: 20px;
`;

export const Cancel = () => {
  return (
    <Icon src={ removeURL } />
  )
};

export const Reset = () => {
    return <Icon src={ resetURL } />
};

export const Check = () => {
    return <Icon src={ checkURL } />
};
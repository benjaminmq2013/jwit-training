
import styled from "styled-components"
import { useState } from 'react';


export const Container = styled.div<{visible: boolean}>`
border-radius: 50%;
border: 1.8px solid white;
height: 35px;
width: 35px;
cursor: pointer;
background-color: ${props => props.visible ? "white" : "transparente"};
display: flex;
align-items: center;
justify-content: center;
color: ${ props => props.visible? "#21c0d0" :"transparent" };
font-size: 20px;
font-weight: 800;
box-shadow: 0px 2px 30px #5d5d5d60;
`
export interface params {
    onClick?: (value:boolean) => void,
};


const App = (params:params):JSX.Element => {
  const [Visible, SetVisible] = useState<boolean>(true)
  
    const handleClick: () => void = () => {
      console.log("Estado Real: ", Visible)
      SetVisible(!Visible)
      if(typeof params.onClick === "function") params.onClick(Visible)
      
    }
  return (
    <Container visible={ !Visible } onClick = { handleClick }>
        ✓
    </Container>
  )
}

export default App
import Option, { params as ParamsComponentOption } from "./Option";
import styled from "styled-components"
import Footer from "./Footer"
import { useState, useEffect } from 'react';

const Container = styled.div`
  margin-top: 15px;
  background-color: ${(props) => props.color};
  border-radius: 15px;

  width: 400px;
  padding-top: 15px;
`


export interface select {
  value: string,
  state: boolean
}

export interface params{
  options: string[]
  backgroundColor?: string,
  onAccept?: (options: select[] ) => void
}


const App = (params:params): JSX.Element => {
  
  const [ optionsState, setOptionsState ] = useState<select[]>([])

  useEffect(() => {
    let raw:select[] = [];

    params.options.forEach(el => raw.push({value: el, state: false}))

    setOptionsState(raw)

  }, [params.options])

  const handleClick:ParamsComponentOption["onClick"] = (e) => {

    const copy = [...optionsState]

    const index:number = copy.findIndex((f) => f.value === e.value);

    if (index > -1) copy[index] = e;
    
    setOptionsState(copy)
    
  }

  const handleAccept = () => {
    // PUNTO IMPORTANTE PARA DEBUGGER
    if (typeof params.onAccept === "function")params.onAccept(optionsState) // PUNTO IMPORTANTE DEBUGGER
    // PUNTO IMPORTANTE PARA DEBUGGER
  }
  const handleReset = () => {
    let raw: select[] = [];

    params.options.forEach(el => raw.push({value: el, state: false}))

    setOptionsState(raw)
  }

  return (
    <Container color={ params.backgroundColor }>
      {params.options.map((v, i)=> <Option key={i} value={v} onClick={ handleClick } />)}
      <Footer onAccept={handleAccept} onReset={ handleReset } />
    </Container>
  )
}

App.defaultProps = {
  options: ["lup nutrition",
  "asitis",
  "avatar",
  "big musclesularity",
  "bpi sports",
  "bsn",
  "cellucor",
  "domin8r",],
  backgroundColor: "#ff7745"
}

export default App
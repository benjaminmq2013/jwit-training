//1:01:49

import styled from "styled-components";

export const Container = styled.div`
  border-radius: 50%;
  border: 1.8px solid white;
  height: 45px;
  width: 45px;
  cursor: pointer;
  background-color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  color: transparent;
  font-size: 20px;
  font-weight: 800;
  box-shadow: 0px 2px 30px #5d5d5d60;
  margin: 0 5px;
`;
export interface params {
  onClick?: () => void;
  element: JSX.Element;
}

const App = (params: params): JSX.Element => {

  const handleClick: () => void = () => {
    
   if( typeof params.onClick === "function" ) params.onClick()
  };
  return (
    <Container onClick={handleClick}>
      {params.element}
    </Container>
  );
};

export default App;

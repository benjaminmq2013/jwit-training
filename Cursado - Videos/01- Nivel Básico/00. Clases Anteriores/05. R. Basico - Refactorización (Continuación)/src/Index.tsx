import styled from "styled-components"
import Menu from "./Menu"

const Container = styled.div`
`


function App(): JSX.Element {
  return (
    <Container>
      <Menu onAccept = {e => console.log(e)}  />
      <Menu
        backgroundColor="#21d0d0"
        options={["PRICE LOW TO HIGH", "PRICE HIGH TO LOW", "popularity"]}
        onAccept={ e => console.log(e) }
      />
    </Container>
  );
}

export default App

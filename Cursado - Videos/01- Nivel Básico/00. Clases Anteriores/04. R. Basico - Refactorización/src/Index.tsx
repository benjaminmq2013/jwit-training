import styled from "styled-components"
import Option, { params } from "./Menu/Option";

const Container = styled.div`
  background-color: lightblue;
`


function App(): JSX.Element {

  const options: string[] = [
    "lup nutrition",
    "asitis",
    "avatar",
    "big musclesularity",
    "bpi sports",
    "bsn",
    "cellucor",
    "domin8r",
  ];

  const handleClick:params["onClick"] = (value) => {

    console.log(value)

  }

  return (
    <Container>
      {options.map((v, i)=> <Option key={i} value={v} onClick={ handleClick } />)}
    </Container>
  );
  
}

export default App

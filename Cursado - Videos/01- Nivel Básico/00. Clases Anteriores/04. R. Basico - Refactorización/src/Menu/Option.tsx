import styled from "styled-components"
import Checkbox from "./Checkbox"

const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`
const Title = styled.p`
    margin: 0px;
    color: white;
    font-weight: 500;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
   /* opacity: 0.8; */
`

export interface params {
    value: string,
    onClick?: (params:{ value: string, state: boolean }) => void
};
const Options = (params:params): JSX.Element => {

    const handleClick = (state: boolean) => {

        if(typeof params.onClick === "function") params.onClick({ value: params.value, state })
    
    }

  return (
    <Container>
        
        <Title>
            { params.value }
        </Title>

        <Checkbox onClick={ handleClick } />

        
    </Container>
  )
}

export default Options
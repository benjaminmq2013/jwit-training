import styled from "styled-components"
import { useState } from 'react';


export const Container = styled.div<{visible: boolean}>`
border-radius: 50%;
border: 1.8px solid white;
height: 35px;
width: 35px;
cursor: pointer;
background-color: ${props => props.visible ? "white" : "transparente"};
display: flex;
align-items: center;
justify-content: center;
color: ${ props => props.visible? "#21c0d0" :"transparent" };
font-size: 20px;
font-weight: 800;
box-shadow: 0px 2px 30px #5d5d5d60;
`
export interface params {
    onClick?: (value:boolean) => void
};



const App = (params:params):JSX.Element => {
  const [visible, SetVisible] = useState<boolean>(false)
  
    const handleClick: () => void = () => {
      SetVisible(!visible)
      if(typeof params.onClick === "function") params.onClick(visible)
      
    }
  return (
    <Container visible={ visible } onClick = { handleClick }>
        ✓
    </Container>
  )
}

export default App
// ME QUEDÉ MINUTO 43:50
import styled from "styled-components";
import Checkbox from "./Checkbox"

import { Cancel, Reset, Check } from "./Buttons"
import { useEffect, useState } from "react";


const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding-top: 5px;
  padding-bottom: 25px;
`;
const ContainerV1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding-top: 5px;
  padding-bottom: 25px;
  border-top: "1px solid #ffffff80";
`;

interface option{
  element: JSX.Element,
  onClick: () => void;
}

export interface params {
  enableReset: boolean;
  enableCancel: boolean;
  enableAccept: boolean;
};

const App = (params:params):JSX.Element => {

  const handleReset = () => {
    console.log("Vamos a hacer reset!")
    if(typeof onReset === "function" ) onReset()
  };

  const handleOk = () => {
    console.log("Vamos a hacer I agree!")
    if(typeof onReset === "function" ) onAccept()
  };

  const handleCancel = () => {
    console.log("Vamos a hacer cancel !")
    if(typeof onReset === "function" ) onCancel()
  };

  
  let data:option[] = [{ 
    element: <Cancel />, 
    onClick:()=>{handleCancel()} 
  },

  { 
    element: <Reset />,
    onClick:()=>{handleReset()} 
  },

  { 
    element: <Check />, 
    onClick:()=>{handleOk()} 
  }
]
  
  return (
    <ContainerV1 >
        {data.map((v, i) => <Checkbox state={ false } onClick={ v.onClick } /> )}
         
      </ContainerV1>
  )
}

export default App
import styled from "styled-components"
import eyes from "./images/eyes.svg"
import ComponentA from "./ComponenteA"
import { Navbar } from "./Navbar/Navbar";
import { Menu } from "./Menu"

const Container = styled.div`
  width: 50%;
`

const Grid = styled.div`
  display: flex;
`


function App() {
  return (
    <>
      <Navbar />

      <Grid>
        <Container>
          <Menu />
        </Container>

        <Container>
          <ComponentA icon={eyes} colorIcon="#fff6e5" />
          <ComponentA icon={eyes} colorIcon="#ebfcf1" />
          <ComponentA icon={eyes} colorIcon="#f4f2ff" />
        </Container>
      </Grid>
    </>
  );
}

export default App;

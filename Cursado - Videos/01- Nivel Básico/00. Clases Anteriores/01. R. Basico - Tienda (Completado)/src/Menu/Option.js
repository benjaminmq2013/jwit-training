import React from 'react'
import styled from "styled-components"
import image from "../images/Options/empty.svg"
import Arrow from "../images/right.svg"

const Opt = styled.div`
  width: 100%;
  height: 75px;
  display: flex;
  align-items: center;
  cursor: pointer;
  border-bottom: 2px solid #FAFAFC;
  &:hover{
    background-color: #FAFAFC;
  }
`
const Title = styled.h4`
  font-weight: 600;
  margin: 0;
  color: rgb(101, 114, 144);
  margin-left: 20px;
`
const Icon = styled.img`
  height: 29px;
  max-width: 20px;
  margin-left: 20px;
`
const RightIcon = styled.img`
  height: 13px;
`
const Figure = styled.figure`
  margin: 0;
  margin-left: auto;
  margin-right: 15px;
  height: 20px;
  width: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 20px;
  background-color: #FAFAFC;
`

export const Option = ({ iconURL, title }) => {
  const src = iconURL || image;
  console.log( Arrow )
  
  return (
    <Opt>
      <Icon src={ src }/>
      <Title>{ title }</Title>

      <Figure>
        <RightIcon src={ Arrow }/>
      </Figure>
    </Opt>
  )
}

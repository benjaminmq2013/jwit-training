import React from 'react'
import styled from 'styled-components'
import { Option } from './Option'

import shoppingCart from "../images/Options/shopping-cart.svg"
import analysis from "../images/Options/analysis-chart.svg"
import checklist from "../images/Options/checklist.svg"
import offer from "../images/Options/discount-price-sale-offer.svg"
import clients from "../images/Options/hanshake-deal.svg"

const Wrapper = styled.div`
  margin: 0;
  box-shadow: 0px 0px 6px #E7E7EC;
  width: calc(100%-70px); // This is full width 
  margin-left: 20px;
  margin-right: 50px;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`

const data = [
  { title: "Pedidos", iconURL: shoppingCart },
  { title: "Analítica de negocio", iconURL: analysis },
  { title: "Clientes", iconURL: clients },
  { title: "Descuentos", iconURL: offer },
  { title: "Formularios Recibidos", iconURL: checklist }
]

export const Menu = () => {
  
  return (
    <Wrapper>
      {data.map((option, i) => (       
        <Option 
          title={ option.title } 
          iconURL={ option.iconURL } 
          key={ i } 
        />
      ))}
    </Wrapper>
  );
}

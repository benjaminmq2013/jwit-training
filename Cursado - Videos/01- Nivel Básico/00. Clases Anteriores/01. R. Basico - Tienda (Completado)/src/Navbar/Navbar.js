import React from 'react'
import styled from 'styled-components'
import { NavbarLink } from './NavbarLink'

const Nav = styled.nav`
    height: 80px;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    display: flex;
    align-items: center;
`

export const Navbar = () => {
  return (
    <Nav>
        <NavbarLink src="/" title="Resumen">Resumen</NavbarLink>
        <NavbarLink src="/admin" title="Administración" >Resumen</NavbarLink>
        <NavbarLink src="config" title="Configuración de la tienda" >Resumen</NavbarLink>
    </Nav>
  )
}

import React from 'react'
import "./ActiveLink.css"
import { NavLink } from "react-router-dom"



  const LinkStyle = {
    color: "#657290",
    fontWeight: "bold",
    marginLeft: "20px",
    paddingBottom: "10px",
    textDecoration: "none",
    // isActive? color: 'red';
  }
  const ActiveLink = {
    borderBottom: "2px solid red",
  }

export const NavbarLink = ({ src, title }) => {
  return (
    <NavLink
      style={LinkStyle}
      className={({ isActive }) => (isActive ? "ActiveLink" : undefined)}
      to={src}
    >
      {title}
    </NavLink>
  );
};

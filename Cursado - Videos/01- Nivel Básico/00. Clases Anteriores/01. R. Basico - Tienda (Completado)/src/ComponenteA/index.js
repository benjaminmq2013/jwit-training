import styled from "styled-components"

import Icon from "./Icon"
import Body from "./Body"

const Container = styled.div`
  height: 80px;
  width: 350px;
  /* background-color: aliceblue; */
  display: flex;
  flex-direction: row;
  margin-top: 15px;
`


function App( params ) {
  return (
    <Container>
      <Icon icon={ params.icon } colorIcon={ params.colorIcon }/>
      <Body />
    </Container>
  );
}

export default App;

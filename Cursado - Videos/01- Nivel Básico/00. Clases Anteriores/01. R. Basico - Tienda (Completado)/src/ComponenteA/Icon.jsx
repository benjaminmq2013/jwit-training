import styled from "styled-components"



const Container = styled.div`
  width: 25%;
  
`
const Body = styled.div`
  background-color: ${props =>  props.colorIcon };
  height: 100%;
  width: 100%;
  border-radius: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
`
const Icons = styled.img`
  height: 35px;
`

function Icon({ icon, colorIcon }) {
  return (
    <Container>
        <Body colorIcon={ colorIcon }>

          <Icons src={ icon } />

        </Body>
      
    </Container>
  );
}

export default Icon;

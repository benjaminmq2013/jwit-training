import styled from "styled-components"


const Container = styled.div`
  height: 100%;
  width: 75%;
  padding-left: 25px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
`;

const Title = styled.p`
    margin: 0px;
    color: #657290;
    font-weight: bold;
    font-size: 15px;
`
const Description = styled.p`
    margin: 0px;
    color: #828b92;
    font-size: 14px;

`
const Enlace = styled.a`
    margin: 0px;
    color: #828b92;
    margin-top: 5px;
    font-size: 14px;
    color: #3b8699;
`

function App({ colorIcon }) {
  return (
    <Container>
        <Title>
            Paquete Premium
        </Title>

        <Description>
            Descubre nuevas funciones
        </Description>

        <Enlace>
            Hazte premium
        </Enlace>
    </Container>
  );
}

export default App;

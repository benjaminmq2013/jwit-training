// voy minuto 1:13:56
import { useState } from "react"
import styled from "styled-components"
import Card from "./Card"
import Submenu from "./Submenu/index"

const Container = styled.div``
const Title = styled.div``
const Button = styled.div``

const InitialState = [
  { 
    id: 1,
    color: "#21c0d0", 
    option: [
      {title: "PRICE LOW TO HIGH", state: false},
      {title: "PRICE HIGH TO LOW", state: false},
      {title: "popularity", state: false}
    ],
    footer: 2
},
  { 
    id: 2,
    color: "#ff7745", 
    option: [
      {title: "lup nutrition", state: false},
      {title: "asitis", state: false},
      {title: "avatar", state: false},
      {title: "big musclesularity", state: false},
      {title: "bpi sports", state: false},
      {title: "bsn", state: false},
      {title: "cellucor", state: false},
      {title: "domin8r", state: false}
    ],
    footer: 1
  }
];

function App() {
  const [data, setData] = useState(InitialState);

  const handleChange = (value, id) => {
    const match = data.filter((el) => el.id === id)[0];

    const act = match.option.filter((el) => el.title === value)[0];

    const copy = [...match.option];

    const targetIndex = copy.findIndex((f) => f.title === value);

    if (targetIndex > -1) {
      copy[targetIndex] = { title: value, state: !act.state };

      // setData(copy)
      match.option = copy;
    }

    const copyList = [...data];
    const targetIndexList = copyList.findIndex((f) => f.title === id);

    if (targetIndexList > -1) {
      copy[targetIndexList] = match;
    }
    // console.log(copyList)
    setData(copyList);
  };



  const handleReset = (id) => {
    console.log(id)
    const copy = [...data];
    const targetIndex = copy.findIndex((f) => f.id === id);

    if (targetIndex > -1) {
      
      let raw = [];

      const options = data.filter(el => el.id === id)[0].option

      for (let x of options) raw.push({ title: x.title, state: false })

      copy[targetIndex].option = raw;
    }
    // console.log(copyList)
    setData(copy);
  }
  return (
    <Container>
      <Card />

      {data.map((v, i) => (
        <Submenu {...v} key={i} onChange={handleChange} onReset={ handleReset } />
      ))}
    </Container>
  );
}

export default App;

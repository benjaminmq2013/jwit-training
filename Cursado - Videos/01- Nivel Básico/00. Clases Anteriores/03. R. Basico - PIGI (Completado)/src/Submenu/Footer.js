import styled from "styled-components";
import { Circulo } from "./Options";
import { Cancel, Reset, Check } from "./Buttons"
import { useState } from "react";

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding-top: 5px;
  padding-bottom: 25px;
`;
const ContainerV1 = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 15px;
  padding-top: 5px;
  padding-bottom: 25px;
  border-top: "1px solid #ffffff80";
`;


  const Item = ({ v }) => {
    const { element, onClick } = v;
    return (
      <Circulo
        visible={true}
        style={{
          height: "50px",
          width: "50px",
          marginLeft: "3px",
          marginRight: "3px",
        }}
        onClick={onClick}
      >
        { element() }
      </Circulo>
    );
  };



const Footer = ({ version, onReset, onCancel, onAccept }) => {

  const handleReset = () => {
    console.log("Vamos a hacer reset!")
    if(typeof onReset === "function" ) onReset()
  };

  const handleOk = () => {
    console.log("Vamos a hacer I agree!")
    if(typeof onReset === "function" ) onAccept()
  };

  const handleCancel = () => {
    console.log("Vamos a hacer cancel !")
    if(typeof onReset === "function" ) onCancel()
  };


  if( version === 1 ){

    let data = [{ 
        element: Cancel, 
        onClick:()=>{handleCancel()} 
      },

      { 
        element: Reset,
        onClick:()=>{handleReset()} 
      },

      { 
        element: Check, 
        onClick:()=>{handleOk()} 
      }
    ]
    
    return (
      <ContainerV1 >
        {data.map((v, i) => <Item v = { v } key={ i } />)}
         
      </ContainerV1>
    );
  } else if (version === 2){
    let data = [{ 
      element: Cancel, 
      onClick:()=>{handleCancel()} 
    },

    { 
      element: Check, 
      onClick:()=>{handleOk()} 
    }
  ]
    return (
      <Container>
        {data.map((v, i) => <Item v = { v } key={ i }  />)}
      </Container>
    );
  }
  
};

export default Footer;

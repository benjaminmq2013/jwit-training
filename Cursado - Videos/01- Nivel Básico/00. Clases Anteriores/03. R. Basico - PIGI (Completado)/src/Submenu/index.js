import React from 'react'
import styled from "styled-components"
import Options from "./Options"
import Footer from "./Footer"

const Container = styled.div`
  margin-top: 15px;
  background-color: ${(props) => props.color};
  border-radius: 15px;
  /* height: 200px; */
  width: 400px;
  padding-top: 15px;
`;


const index = (params) => {
  const handleChange = (value) => {
    if(typeof params.onChange === "function" ) params.onChange( value, params.id )
  }
  const handleReset = () => {
    if(typeof params.onReset === "function" ) params.onReset( params.id )
  }
  return (
    <Container color={params.color}>
        {
          params.option.map((opt, i) => <Options {...opt} key={ i } onChange={ handleChange } />)
        }
        <Footer version = { params.footer } onReset={handleReset} /*onReset onCancel onAccept*/ />
    </Container>
  )
}

export default index


import styled from "styled-components";

import removeURL from "../images/remove-icon.svg";
import resetURL from "../images/reset-icon.svg";
import checkURL from "../images/check-icon.svg";

const Icon = styled.img`
  height: 20px;
  width: 20px;
`;

export const Cancel = () => {
  return <Icon src={ removeURL } />;
};

export const Reset = () => {
    return <Icon src={ resetURL } />
};

export const Check = () => {
    return <Icon src={ checkURL } />
};
import styled from "styled-components"
import Card from "./Card"
import Submenu from "./Submenu/index"

const Container = styled.div``
const Title = styled.div``
const Button = styled.div``


function App() {
  return (
    <Container>
      <Card/>

      <Submenu />
    </Container>
  );
}

export default App;

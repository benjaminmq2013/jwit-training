import React from 'react'
import styled from "styled-components"
import Options from "./Options"
import { Circulo } from './Options'

const Container = styled.div`
    background-color: #21d0d0;
    border-radius: 15px;
    height: 200px;
    width: 400px;
    padding-top: 15px;

    /* opacity: 0.8; */
`
const Footer = styled.footer`
    display: flex;
    justify-content: center;
    align-items: center;

`

const index = () => {
    const options = [
        {title: "PRICE LOW TO HIGH", state: false},
        {title: "PRICE HIGH TO LOW", state: false},
        {title: "popularity", state: true}
    ]
  return (
    <Container>
        {
            options.map((opt, i) => <Options {...opt} key={ i } />)
        }
        <Footer>

            {
                [ "✓", "×" ].map((v, i) => {
                    return (
                      <Circulo
                        visible={true}
                        style={{ height: "50px", width: "50px", marginLeft:"3px", marginRight:"3px"}}
                        key={ i }
                      >
                        { v }
                      </Circulo>
                    );
                })
            }

            
          
        </Footer>
    </Container>
  )
}

export default index
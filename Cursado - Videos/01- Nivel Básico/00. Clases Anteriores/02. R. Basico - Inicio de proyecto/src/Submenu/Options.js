import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
const Container = styled.div`
    margin-left: 25px;
    margin-top: 15px;
    display: flex;
    justify-content: space-between;
    margin-right: 25px;
    align-items: center;
`
const Title = styled.p`
    margin: 0px;
    color: white;
    font-weight: 500;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
   /* opacity: 0.8; */
`
export const Circulo = styled.div`
    border-radius: 50%;
    border: 1.8px solid white;
    height: 35px;
    width: 35px;
    cursor: pointer;
    background-color: ${props => props.visible ? "white" : "transparente"};
    display: flex;
    align-items: center;
    justify-content: center;
    color: #21d0d0;
    font-size: 20px;
    font-weight: 800;
    box-shadow: 0px 2px 6px #bababa;
`

const Options = ({ title, state }) => {

    const [visible, setVisible] = useState(false)

    useEffect(() => {
      
        if(state) setVisible(true)
        else setVisible(false)

    }, [state])
    
    const handleChange = () => {
        setVisible(!visible)
    }

  return (
    <Container>
        <Title>
            { title }
        </Title>

        <Circulo visible={ visible } onClick = {handleChange}>
            ✓
        </Circulo>
    </Container>
  )
}

export default Options
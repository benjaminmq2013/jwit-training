import { useState } from "react"
import Card, { params as paramsCard } from "./components/Card"
import Grid from "./components/Grid"

const App = ():JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    { id:"1", image: "/vite.svg", title: "Tienda 1" },
    { id:"2", image: "/vite.svg", title: "Tienda 2" },
    { id:"3", image: "/vite.svg", title: "Tienda 3" },
    { id:"4", image: "/vite.svg", title: "Tienda 4" },
    { id:"5", image: "/vite.svg", title: "Tienda 5" },
    { id:"6", image: "/vite.svg", title: "Tienda 6" },
    { id:"7", image: "/vite.svg", title: "Tienda 7" },
    { id:"8", image: "/vite.svg", title: "Tienda 8" },
  ]);

  const handleSelect:paramsCard["onClick"] = (e) => {
    console.log(e)
  }
  
  return (
    <Grid>
      {data.map(v => (
        <Card {...v} key={ v.id } onClick={ handleSelect } ></Card>
      ))}
    </Grid>
  )
}

export default App
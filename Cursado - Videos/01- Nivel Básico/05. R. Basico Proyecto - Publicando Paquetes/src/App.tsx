import * as React from 'react';
import { useState } from "react"
import Card, { params as paramsCard } from "@react/card-template"
import CardProduct from "@react/product"
import Grid from "@react/grid"

const App = ():JSX.Element => {

  const [data, setData] = useState<paramsCard[]>([
    { id:"1", image: "/vite.svg", title: "Tienda 1" },
    { id:"2", image: "/vite.svg", title: "Tienda 2" },
    { id:"3", image: "/vite.svg", title: "Tienda 3" },
    { id:"4", image: "/vite.svg", title: "Tienda 4" },
    { id:"5", image: "/vite.svg", title: "Tienda 5" },
    { id:"6", image: "/vite.svg", title: "Tienda 6" },
    { id:"7", image: "/vite.svg", title: "Tienda 7" },
    { id:"8", image: "/vite.svg", title: "Tienda 8" },
  ]);

  const handleSelect:paramsCard["onClick"] = (e) => {
    console.log(e)
  }
  
  return (
    <Grid>
      <CardProduct
        value="1"
        image="iphone13.png"
        title="Telefono"
        description=""
        onMouseOver={ ()=> console.log("over") }
        onMouseLeave={ ()=> console.log("Leave") }
      />
      <CardProduct
        value="2"
        image="iphone13-reverse.png"
        title="Telefono"
        description=""
      />
      <>
        {data.map((v) => (
          <Card {...v} key={v.id} onClick={handleSelect}></Card>
        ))}
      </>
    </Grid>
  );
}

export default App
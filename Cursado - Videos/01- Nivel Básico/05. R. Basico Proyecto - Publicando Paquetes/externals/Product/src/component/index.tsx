import React from 'react'
import styled from "styled-components"

const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 150px;
`
const Img = styled.img`
    aspect-ratio: 9/16;
    object-fit: cover;
    /* height: 150px; */
`

const Body = styled.div`
    display: flex;
    flex-direction: column;
    padding-left: 10px;
    padding-right: 10px;
    background-color: #1c1719;
    border-radius: 5px;
`
const Title = styled.p`
    color: #fefefe;
`
const Description = styled.p`
    
`

export interface params{
    value: string,
    image: string,
    title: string,
    description: string,
    onClick?: (value:string) => void,
    onMouseLeave?: React.MouseEventHandler<HTMLDivElement>,
    onMouseOver?: React.MouseEventHandler<HTMLDivElement>,
}

const defaultProps = {
    // title: "Product 1",
    // image: "https://images.pexels.com/photos/1030986/pexels-photo-1030986.jpeg?cs=srgb&dl=pexels-daria-shevtsova-1030986.jpg&fm=jpg",
    // description: ""
}

/**
 * 
 * @param params 
 * @param params.value key of product
 *  
 * @example
 * 
 * 
    import CardProduct from "@react/product"

    const App = ():JSX.Element => {

    
    return (
        
            <CardProduct
                value="1"
                image="iphone13.png"
                title="Telefono"
                description=""
                onMouseOver={ ()=> console.log("over") }
                onMouseLeave={ ()=> console.log("Leave") }
            />
        
    );
    }

    export default App
 * 
 * @returns JSX.Element
 */

const App = (params:params):JSX.Element => {

    params = { ...defaultProps, ...params }

    const handleClick = () => {
        typeof params.onClick === "function" && params.onClick(params.value)
    }

  return (
    <Container onClick={ handleClick } onMouseLeave={ params.onMouseLeave } onMouseOver={ params.onMouseOver } >
        <Img src={ params.image } />

        <Body>

            <Title>{ params.title }</Title>
            <Description>{ params.description }</Description>

        </Body>
    </Container>
  )
}



export default App
import React from 'react'
import styled from "styled-components";
import Avatar from "@react/avatar";

const Container = styled.div`
    display: flex;
    flex-direction:column;
    padding: 30px 15px;
    width: 123px;
    height: 160px;
    align-items: center;
    justify-content:center;
    border-radius: 7px;
    color: white;
    gap: 15px;
`
const Title = styled.p`
    
`

export interface params{
    id:string,
    title: string,
    description?: string,
    image: string,
    onClick?: (id:string) => void;
}

const Description = styled.p`
    font-size: 12px;
`
/**
 * 
 * @param params 
 * @param params.id Identificador del elemento
 * @param params.titulo Titulo del elemento
 * @param params.description Descripción del elemento
 * @param params.onClick callback que retorna un identificador al presionar sobre el componente.
 * 
 * @example  
 * import { useState } from "react"
 * import Card, { params as paramsCard } from "./components/Card"
 * import Grid from "./components/Grid"

 * const App = ():JSX.Element => {

 * const [data, setData] = useState<paramsCard[]>([
    { id:"1", image: "/vite.svg", title: "Tienda 1" },
    { id:"2", image: "/vite.svg", title: "Tienda 2" },
    { id:"3", image: "/vite.svg", title: "Tienda 3" },    
  ]);
  
  return (
    <Grid>
      {data.map((v, i) => (
        <Card {...v} key={ i } onClick={  } ></Card>
      ))}
    </Grid>
  )
}

export default App
 * 
 * @returns JSX.Element
 */
const App = (params:params):JSX.Element => {

  const handleClick = () => {
    if(typeof params.onClick === "function") params.onClick(params.id)
  }

  return (
    <Container onClick={ handleClick }>

        <Avatar image={ params.image } />

        <Title>{ params.title }</Title>

        <Description>{ params.description }</Description>
        
    </Container>
  )
}

export default App
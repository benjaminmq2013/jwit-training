import React, { ReactNode } from 'react';
import styled, { CSSProperties } from 'styled-components';

const Container = styled.div`
    position: relative;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content:space-evenly;
    align-self: center;
    align-content: flex-start;
    display: flex;
    width: 100%;
    height: 100%;
    overflow: auto;
    gap: 15px;
`

export interface params{
    children: ReactNode;
    style?: CSSProperties,
}

const App = (params:params):JSX.Element => {
    return (
        <Container style={ params.style }>
            { params.children }
        </Container>
    )
}

export default App